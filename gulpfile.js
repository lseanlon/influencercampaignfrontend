var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var reload = browserSync.reload;

var INPUT_PATH = 'src';
var OUTPUT_PATH = 'dist';

gulp.task('watch', function() {
    gulp.watch(INPUT_PATH + 'sass/**/*.scss', ['styles']);
    gulp.watch('*.html').on('change', reload);
    gulp.watch(INPUT_PATH +'script/**/*.js', ['browserify']) ;
})

gulp.task('serve', ['browserify','sass','watch'], function() {
    browserSync.init({
        server: './' + INPUT_PATH
    }); 
   
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src(INPUT_PATH + '/sass/*.scss')
        .pipe(sass())
        .pipe(gulp.dest(OUTPUT_PATH + '/css'))
        .pipe(browserSync.stream());
});

gulp.task('styles', function() {
    return gulp.src(INPUT_PATH + 'sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./' + OUTPUT_PATH + '/css/'))
        .pipe(browserSync.stream());
});


gulp.task('browserify', function() {
    // Grabs the app.js file
    return browserify('./' +INPUT_PATH  + '/script/app.js')
        // bundles it and creates a file called main.js
        .bundle()
        .pipe(source('main.js'))
        // saves it the public/js/ directory
        .pipe(gulp.dest('./' +OUTPUT_PATH + '/script/'));
})
 

gulp.task('default', ['serve']);
