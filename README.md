----GENERAL INTRODUCTION----
Influencer Campaign Front End

The stacks used are
- npm
- gulp
	- scss SASS [concatenation css] 
	- browser sync
	- javascript [minification js]
- jquery 
- angularjs


----INSTALLATION GUIDE----
1. Install npm if you havent already.
2. Go into directory and run following command in queue.
	- 'npm cache clean'
	- 'npm install' OR 'npm install gulp' ,'npm install gulp-sass','npm install browser- sync'
	-  'npm rebuild'
	- 'gulp serve'
	 
3. Run 'gulp serve' to begin your work
